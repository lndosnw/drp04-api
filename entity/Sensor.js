const { Sequelize, sequelize } = require('../sql/Conexao');

const Model = Sequelize.Model;
class Sensor extends Model {}
Sensor.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        descricao: {
            type: Sequelize.STRING(100),
            allowNull: false,
        },
        macaddress: {
            type: Sequelize.STRING(20),
            allowNull: false
        }
    },
    {
        sequelize,
        modelName: "sensor",
        tableName: "sensor",
        schema:"sensor",
        // options

        createdAt: false,
        updatedAt: false,
    }
);


module.exports = Sensor;
