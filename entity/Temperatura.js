const { Sequelize, sequelize } = require('../sql/Conexao');

const Model = Sequelize.Model;
class Temperatura extends Model {}
Temperatura.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        sensor:{
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        temperatura: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        dataRegistro: {
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: new Date(),
        }
    },
    {
        sequelize,
        modelName: "temperatura",
        tableName: "temperatura",
        schema:"sensor",
        // options

        createdAt: false,
        updatedAt: false,
    }
);


module.exports = Temperatura;
