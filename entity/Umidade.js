const { Sequelize, sequelize } = require('../sql/Conexao');

const Model = Sequelize.Model;
class Umidade extends Model {}
Umidade.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        sensor:{
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        umidade: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        dataRegistro: {
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: new Date(),
        }
    },
    {
        sequelize,
        modelName: "umidade",
        tableName: "umidade",
        schema:"sensor",
        // options

        createdAt: false,
        updatedAt: false,
    }
);


module.exports = Umidade;
