const { Sequelize, sequelize } = require('../sql/Conexao');

const Sensor = require('../entity/Sensor');

async function getList(){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.findAll();
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function getSensorPorID(id){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.findOne({where:{id:id}});
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function getSensorPorMac(mac){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.findOne({where:{macaddress:mac}});
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function deleteID(id){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.destroy({where:{id:id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function insert(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.create(obj);

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function edit(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const sensor = await Sensor.update(obj,{where:{id:obj.id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return sensor;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

module.exports={
    getList,getSensorPorID,getSensorPorMac,deleteID,insert,edit
}
