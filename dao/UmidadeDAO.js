const { Sequelize, sequelize } = require('../sql/Conexao');

const Umidade = require('../entity/Umidade');

async function getList(){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.findAll();
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function getUmidadePorSensor(sensor){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.findAll({where:{sensor:sensor}});
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function deleteID(id){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.destroy({where:{id:id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function deleteSensor(sensor){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.destroy({where:{sensor:sensor}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function insert(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.create(obj);

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function edit(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const umidade = await Umidade.update(obj,{where:{id:obj.id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return umidade;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

module.exports={
    getList,getUmidadePorSensor,deleteID,deleteSensor,insert,edit
}
