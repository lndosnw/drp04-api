const { Sequelize, sequelize } = require('../sql/Conexao');

const Temperatura = require('../entity/Temperatura');

async function getList(){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.findAll();
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function getTemperaturaPorSensor(sensor){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.findAll({where:{sensor:sensor}});
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function deleteID(id){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.destroy({where:{id:id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function deleteSensor(sensor){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.destroy({where:{sensor:sensor}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function insert(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.create(obj);

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

async function edit(obj){
    return await sequelize
        .authenticate()
        .then(async () => {
            const temperatura = await Temperatura.update(obj,{where:{id:obj.id}});

            //console.log("getUnidadePorDescricao usuarioDocumento_Arquivos_Tipos tipo = "+usuarioDocumento_Arquivos_Tipos.tipo);
            return temperatura;
        })
        .catch((err) => {
            console.error("Unable to connect to the database:", err);
        });
}

module.exports={
    getList,getTemperaturaPorSensor,deleteID,deleteSensor,insert,edit
}
