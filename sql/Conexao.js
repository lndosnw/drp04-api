//const pg = require('pg');
const globais = require("../util/Globais");

//configuracao
const gbh = globais.getCredenciaisBD();
const hostBD = gbh.host;
const userBD = gbh.user;
const passwordBD = gbh.password;
const databaseBD = gbh.database;
const portBD = 5432;
const sslBD = false;

const config = {
    hostBD: hostBD,
    // Do not hard code your username and password.
    // Consider using Node environment variables.
    userBD: userBD,
    passwordBD: passwordBD,
    databaseBD: databaseBD,
    portBD: portBD,
    sslBD: sslBD
};

//*************/
//  sequelize  /
//*************/
const Sequelize = require("sequelize");

// Option 1: Passing parameters separately
/*
const sequelize = new Sequelize(databaseBD, userBD, passwordBD, {
    host: hostBD,
    dialect: "postgres",
    dialectOptions: {
        ssl:true,
        rejectUnauthorized: false,
        connectTimeout: 300000,
        statement_timeout: 300000,
        idle_in_transaction_session_timeout: 300000,
        requestTimeout: 300000
    },
    pool: {
        max: 5,
        min: 0,
        idle: 200000,
        // @note https://github.com/sequelize/sequelize/issues/8133#issuecomment-359993057
        acquire: 1000000,
    },
    acquire: 300000,
    handleDisconnects: true
});

 */

const sequelize = new Sequelize({
    database: databaseBD,
    username: userBD,
    password: passwordBD,
    host: hostBD,
    port: portBD,
    dialect: "postgres",
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: false // <<<<<<< YOU NEED THIS
        }
    },
});

/*
const sequelize = new Sequelize("postgres://zodyeaxqhvtouj:6f9a5992f4438e81c0a222400f0644c4f8c9a07b7d4575c1901f57652200217a@ec2-3-218-47-9.compute-1.amazonaws.com:5432/dcu4mb91f49uhq?sslmode=require", {
    dialect: 'postgres',
    protocol: 'postgres',
    dialectOptions: {
        ssl: true,
        rejectUnauthorized: false, // very important
    }
});
 */

module.exports={
    config,
    sequelize,
    Sequelize
}
