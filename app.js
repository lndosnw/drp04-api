
const scheduledFunctions = require("./cronjobs/scheduleFunction");
scheduledFunctions.initScheduledJobs();

const port = 3000;
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const passport = require('passport');
const session = require('express-session');

//midlewares
const authenticationMiddleware = require("./midlewares/authenticationMiddleware");
const verificaTokenMiddleware = require("./midlewares/verifyToken");

var sassMiddleware = require('node-sass-middleware');
const bodyParser = require('body-parser');
const expressLayouts = require('express-ejs-layouts');

/*
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var lojaRouter = require('./routes/loja');
var detalhesRouter = require('./routes/detalhes');
var loginADMRouter = require('./routes/loginADM');
var healthcheckRouter = require('./routes/healthcheck');

var categoriaRouter = require('./routes/categoria');
var mercadoriaRouter = require('./routes/mercadoria');
var painelRouter = require('./routes/painel');
var contatosRouter = require('./routes/contatos');*/

var indexRouter = require('./routes/index');
var healthcheckRouter = require('./routes/healthcheck');
var sensorRouter = require('./routes/sensor');
var register_sensorRouter = require('./routes/register_sensor');

const globais = require('./util/Globais');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts)           // Definimos que vamos utilizar o express-ejs-layouts na nossa aplicação
app.use(bodyParser.urlencoded())  // Com essa configuração, vamos conseguir parsear o corpo das requisições


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));//deixa pasta de uploads publica

//novo auth
require('./midlewares/auth')(passport);
app.use(session({
  secret: '123',//configure um segredo seu aqui,
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 30 * 60 * 1000 }//30min
}))
app.use(passport.initialize());
app.use(passport.session());
require("dotenv-safe").config(); //configura variaveis de ambiente do arquivo .env
//fim novo auth

/*
app.use('/', indexRouter);
app.use('/index', indexRouter);
app.use('/users', usersRouter);
app.use('/loja', lojaRouter);
app.use('/detalhes', detalhesRouter);
app.use('/loginADM', loginADMRouter);
app.use('/healthcheck', healthcheckRouter);

app.use('/adm/categoria',authenticationMiddleware, categoriaRouter);
app.use('/adm/mercadoria',authenticationMiddleware, mercadoriaRouter);
app.use('/adm/painel',authenticationMiddleware, painelRouter);
app.use('/adm/contatos',authenticationMiddleware, contatosRouter);*/

/*app.use('/', indexRouter);
app.use('/index', indexRouter);*/
app.use('/healthcheck', healthcheckRouter);
app.use('/sensor',sensorRouter);
app.use('/register_sensor', register_sensorRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  res.render('error', { layout: 'error'});
});

//module.exports = app;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})