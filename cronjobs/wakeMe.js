async function execute(){
    console.log("executed wakeme");
    try {
        const http = require("https");

        const options = {
        "method": "GET",
        "hostname": "webservice-drp04-pji510-sala-001grupo-014.onrender.com",
        "port": 3000,
        "path": "/healthcheck",
        "headers": {
            "User-Agent": "insomnia/2023.5.8",
            "Content-Length": "0"
        }
        };

        const req = http.request(options, function (res) {
        const chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            const body = Buffer.concat(chunks);
            console.log(body.toString());
        });
        });

        req.end();
    } catch (error) {
        console.log(error);
    }

    console.log("executed wakeme");
    
}

module.exports = {execute};