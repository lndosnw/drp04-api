const CronJob = require("node-cron");

exports.initScheduledJobs = () => {
  const scheduledJobFunction = CronJob.schedule("*/5 * * * *", () => {
    try {
        console.log("Start Schedule!");
        const init = require('./init');
        init.execute();
        console.log("Stop Schedule!");
    } catch (error) {
        console.log(error);
    }
    
  });

  scheduledJobFunction.start();
}