async function execute(){
    console.log("execute schedule init");
    try {
        const wakeSuperSet = require('./wakeSuperSet');
        await wakeSuperSet.execute();
    } catch (error) {
        console.log(error);
    }

    try {
        const wakeMe = require('./wakeMe');
        await wakeMe.execute();
    } catch (error) {
        console.log(error);        
    }
    console.log("executed schedule init");
}

module.exports = {execute};