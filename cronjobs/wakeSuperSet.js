async function execute(){
    console.log("execute wakesuperset");
    try {
        const http = require("https");

        const options = {
        "method": "GET",
        "hostname": "sset-m5sd.onrender.com",
        "port": null,
        "path": "/health",
        "headers": {
            "User-Agent": "insomnia/2023.5.8",
            "Content-Length": "0"
        }
        };

        const req = http.request(options, function (res) {
        const chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            const body = Buffer.concat(chunks);
            console.log(body.toString());
        });
        });

        req.end();
    } catch (error) {
        console.log(error);
    }

    console.log("executed wakesuperset");
}

module.exports = {execute};