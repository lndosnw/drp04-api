var express = require('express');
const sensorDAO = require("../dao/SensorDAO");
const umidadeDAO = require('../dao/UmidadeDAO');
const temperaturaDAO = require('../dao/TemperaturaDAO');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({message: 'Destino inacessivel!'});
});

router.get('/list', function(req, res, next) {
    execGet(req.query).then((data)=> {
      res.status(data ? 200 : 500).json(data);
    });
});

router.get('/get', function(req, res, next) {
    execGet(req.query).then((data)=> {
      res.status(data ? 200 : 500).json(data);
    });
});
  
router.post('/set', function(req, res, next) {
    execPost(req.body).then((data)=> {
        res.status(data ? 200 : 500).json(data);
    });
});
  
router.post('/delete', function(req, res, next) {
    execPost(req.body).then((data)=> {
        res.status(data ? 200 : 500).json(data);
    });
});

async function execGet(req){
    var registros = [];
    
    if(req.mac){
        try{
            const sensores = await sensorDAO.getSensorPorMac(req.mac);

            for(const sensor of sensores){
                const temperaturas = await temperaturaDAO.getTemperaturaPorSensor(sensor.id);
                const umidades = await umidadeDAO.getUmidadePorSensor(sensor.id);
                registros.push({sensor,temperaturas,umidades});
            }
        }catch (e) {
            console.log("erro sensordao.getlist register_sensor/execGet",e);
        }
    }else{
        try{
            const sensores = await sensorDAO.getList();

            for(const sensor of sensores){
                const temperaturas = await temperaturaDAO.getTemperaturaPorSensor(sensor.id);
                const umidades = await umidadeDAO.getUmidadePorSensor(sensor.id);
                registros.push({sensor,temperaturas,umidades});
            }
        }catch (e) {
            console.log("erro sensordao.getlist register_sensor/execGet",e);
        }
    }
    return {
        registros:registros
    }
}
  
async function execPost(req){

    if(req.id){//realiza delecao
        const sensor = await sensorDAO.getSensorPorID(req.id);
        if(sensor){
            const resultUmidade = await umidadeDAO.deleteSensor(sensor.id);
            const resultTemperatura = await temperaturaDAO.deleteSensor(sensor.id);
            const resultSensor = await sensorDAO.deleteID(req.id);
            if(resultUmidade&&resultTemperatura&&resultSensor){return true;}else{return false;}
        }else{return false;}
    }else if(req.mac){
        const sensor = await sensorDAO.getSensorPorMac(req.mac);
        var resultUmidade = false;
        var resultTemperatura = false;
        if(req.umidade){
            try{
                const umidade = {
                    sensor: sensor.id,
                    umidade: req.umidade,
                    dataRegistro: new Date(),
                };
                await umidadeDAO.insert(umidade);
                resultUmidade = true;
            }catch(e){
                console.log("erro umidade salvamento");
            }
        }
        if(req.temperatura){
            try{
                const temperatura = {
                    sensor: sensor.id,
                    temperatura: req.temperatura,
                    dataRegistro: new Date(),
                };
                await temperaturaDAO.insert(temperatura);
                resultTemperatura = true;
            }catch(e){
                console.log("erro temperatura salvamento");
            }
        }

        if(resultTemperatura||resultUmidade){return true;}else{return false;}
    }else{
      //operacao invalida
      return false;
    }

}

module.exports = router;