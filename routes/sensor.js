var express = require('express');
const sensorDAO = require("../dao/SensorDAO");
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({message: 'Destino inacessivel!'});
});

router.get('/list', function(req, res, next) {
    execGet(req.query).then((data)=> {
      res.status(data ? 200 : 500).json(data);
    });
});

router.get('/get', function(req, res, next) {
    execGet(req.query).then((data)=> {
      res.status(data ? 200 : 500).json(data);
    });
});
  
router.post('/set', function(req, res, next) {
    execPost(req.body).then((data)=> {
        res.status(data ? 200 : 500).json(data);
    });
});

async function execGet(req){
    var sensores = [];
    
    if(req.id){
        try{
            sensores = await sensorDAO.getList();
            const sensor = await sensorDAO.getSensorPorMac(req.mac);
            sensores = []; sensores.push(sensor);
        }catch (e) {
            console.log("erro sensordao.getlist register_sensor/execGet",e);
        }
    }else{
        try{
            sensores = await sensorDAO.getList();
        }catch (e) {
            console.log("erro sensordao.getlist register_sensor/execGet",e);
        }
    }
    return {
        sensores:sensores
    }
}
  
async function execPost(req){
    if(req.id){//realiza delecao
      return ((await sensorDAO.deleteID(req.idCategoria))>0);
    }else if(req.nome){//realiza criacao
      const sensor = {
        descricao: req.nome,
        macaddress: req.mac
      };
      const retornoSensor = await sensorDAO.insert(sensor);
      console.log("retornoSensor",retornoSensor);
      return retornoSensor;
    }else{
      //operacao invalida
      return false;
    }
}

module.exports = router;